package eval

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"os"
	"testing"
	"time"

	"bitbucket.org/_metalogic_/glib/date"
)

type Person struct {
	Name        string    `json:"name"`
	Address     *Address  `json:"address"`
	Birthdate   date.Date `json:"birthdate"`
	TimeOfBirth time.Time `json:"timeOfBirth"`
}

func (p *Person) Age() int {
	return date.Age(p.Birthdate.DateTime())
}

func (p *Person) AgeAt(t time.Time) int {
	return date.AgeAt(p.Birthdate.DateTime(), t)
}

func (p *Person) CountryIn(countries ...string) bool {
	for _, country := range countries {
		if p.Address != nil && p.Address.Country == country {
			return true
		}
	}
	return false
}

type Address struct {
	Street  string `json:"street"`
	City    string `json:"city"`
	Region  string `json:"region"`
	Country string `json:"country"`
}

var (
	person   *Person
	nofixed  *Person
	deceased *Person

	valentines2022 = 1.6447968e+18
)

func init() {

	data := `{
		"name": "Jane Smith",
		"address": {
			"street": "123 Main Street",
			"city": "Victoria",
			"region": "British Columbia",
			"country":"Canada"
		},
		"birthdate": "1955-06-29",
		"timeOfBirth": "1955-06-29T11:15:00Z"
	}`

	person = &Person{}

	err := json.Unmarshal([]byte(data), person)
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

	nofixed = &Person{
		Name: "John Doe",
		Address: &Address{
			City:    "Victoria",
			Region:  "British Columbia",
			Country: "Canada",
		},
	}
	deceased = &Person{
		Name: "Jane Doe",
	}

}

// astTest represents a test of expression evaluation
type astTest struct {
	Name       string
	Expression string
	Input      *EvaluationTree
	Functions  map[string]ExpressionFunction
	Parameters []EvaluationParameter
	Expected   interface{}
}

func TestASTNoParameterEvaluation(test *testing.T) {

	arithTests := []astTest{

		{
			Name: "Single PLUS",
			Input: &EvaluationTree{
				symbol:   PLUS,
				operator: operatorEvalMap[PLUS],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(49.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(51.0),
				},
			},
			Expected: 100.0,
		},
		{
			Name: "Single MINUS",
			Input: &EvaluationTree{
				symbol:   MINUS,
				operator: operatorEvalMap[MINUS],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(100.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(51.0),
				},
			},
			Expected: 49.0,
		},
		{

			Name: "Single MULTIPLY",
			Input: &EvaluationTree{
				symbol:   MULTIPLY,
				operator: operatorEvalMap[MULTIPLY],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(5.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(20.0),
				},
			},
			Expected: 100.0,
		},
		{

			Name: "Single DIVIDE",
			Input: &EvaluationTree{
				symbol:   DIVIDE,
				operator: operatorEvalMap[DIVIDE],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(100.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(20.0),
				},
			},
			Expected: 5.0,
		},
		{

			Name: "Single even MODULUS",
			Input: &EvaluationTree{
				symbol:   MODULUS,
				operator: operatorEvalMap[MODULUS],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(100.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(20.0),
				},
			},
			Expected: 0.0,
		},
		{

			Name: "Single odd MODULUS",
			Input: &EvaluationTree{
				symbol:   MODULUS,
				operator: operatorEvalMap[MODULUS],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(101.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(2.0),
				},
			},
			Expected: 1.0,
		},
	}

	runASTEvaluationTests(arithTests, test)

	boolTests := []astTest{

		{
			Name: "TRUE",
			Input: &EvaluationTree{
				symbol:   OR,
				operator: operatorEvalMap[OR],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(true),
				},
			},
			Expected: true,
		},
		{
			Name: "FALSE",
			Input: &EvaluationTree{
				symbol:   AND,
				operator: operatorEvalMap[AND],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(false),
				},
			},
			Expected: false,
		},
		{
			Name: "GT",
			Input: &EvaluationTree{
				symbol:   GT,
				operator: operatorEvalMap[GT],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(49.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(51.0),
				},
			},
			Expected: false,
		},
		{
			Name: "EQ",
			Input: &EvaluationTree{
				symbol:   MINUS,
				operator: operatorEvalMap[EQ],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(100.0),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(100.0),
				},
			},
			Expected: true,
		},
		{

			Name: "Short circuit AND",
			Input: &EvaluationTree{
				symbol:   AND,
				operator: operatorEvalMap[AND],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(false),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(true),
				},
			},
			Expected: false,
		},
		{

			Name: "Short circuit OR",
			Input: &EvaluationTree{
				symbol:   OR,
				operator: operatorEvalMap[OR],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(true),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(false),
				},
			},
			Expected: true,
		},
		{

			Name: "Complex Boolean",
			Input: &EvaluationTree{
				symbol:   AND,
				operator: operatorEvalMap[AND],
				left: &EvaluationTree{
					symbol:   OR,
					operator: operatorEvalMap[OR],
					left: &EvaluationTree{
						symbol:   GT,
						operator: operatorEvalMap[GT],
						left: &EvaluationTree{
							symbol:   LITERAL,
							operator: makeLiteralStage(10.0),
						},
						right: &EvaluationTree{
							symbol:   LITERAL,
							operator: makeLiteralStage(9.0),
						},
					},
					right: &EvaluationTree{
						symbol:   LITERAL,
						operator: makeLiteralStage(false),
					},
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(false),
				},
			},
			Expected: false,
		},
	}

	runASTEvaluationTests(boolTests, test)

}

func TestASTParameterizedEvaluation(test *testing.T) {

	evaluationTests := []astTest{

		{

			Name: "Single parameter modified by constant",
			Input: &EvaluationTree{
				symbol:   PLUS,
				operator: operatorEvalMap[PLUS],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeParameterStage("foo"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(2.0),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 2.0,
				},
			},
			Expected: 4.0,
		},
		{

			Name: "Single parameter modified by variable",
			Input: &EvaluationTree{
				symbol:   MULTIPLY,
				operator: operatorEvalMap[MULTIPLY],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeParameterStage("foo"),
				},
				right: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeParameterStage("bar"),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 5.0,
				},
				{
					Name:  "bar",
					Value: 2.0,
				},
			},
			Expected: 10.0,
		},
		{

			Name: "Single structure field reference",
			Input: &EvaluationTree{
				symbol:   EQ,
				operator: operatorEvalMap[EQ],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Name"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage("Jane Smith"),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{

			Name: "Single structure method reference",
			Input: &EvaluationTree{
				symbol:   GTE,
				operator: operatorEvalMap[GTE],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Age"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(66.0),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Structure method reference with argument",
			Input: &EvaluationTree{
				symbol:   GTE,
				operator: operatorEvalMap[GTE],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.AgeAt"),
					right: &EvaluationTree{
						symbol:   LITERAL,
						operator: makeDatetimeStage("1965-01-01"),
					},
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(9.0),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{

			Name: "Multiple structure field reference",
			Input: &EvaluationTree{
				symbol:   EQ,
				operator: operatorEvalMap[EQ],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Address.City"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage("Victoria"),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{

			Name: "Empty structure field reference",
			Input: &EvaluationTree{
				symbol:   EQ,
				operator: operatorEvalMap[EQ],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Address.Street"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(""),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: nofixed,
				},
			},
			Expected: true,
		},
		{

			Name: "Test reference in list",
			Input: &EvaluationTree{
				symbol:   IN,
				operator: operatorEvalMap[IN],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Address.City"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage([]interface{}{"Vancouver", "Victoria"}),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{

			Name: "Test reference contains in list",
			Input: &EvaluationTree{
				symbol:   CONTAINS_IN,
				operator: operatorEvalMap[CONTAINS_IN],
				left: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeReferenceStage("person.Address.City"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage([]interface{}{"Van", "Vic"}),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{

			Name: "Date comparison",
			Input: &EvaluationTree{
				symbol:   GT,
				operator: operatorEvalMap[GT],
				left: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeDateStage("2022-02-17"),
				},
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeParameterStage("now"),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "now",
					Value: valentines2022,
				},
			},
			Expected: true,
		},
	}
	runASTEvaluationTests(evaluationTests, test)
}

func TestASTFunctionEvaluation(test *testing.T) {

	evaluationTests := []astTest{
		{
			Name: "Zeroary function",
			Input: &EvaluationTree{
				symbol:   FUNCTIONAL,
				operator: makeFunctionStage(one),
			},
			Expected: 1.0,
		},
		{
			Name: "Unary function",
			Input: &EvaluationTree{
				symbol:   FUNCTIONAL,
				operator: makeFunctionStage(round),
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage(1.6),
				},
			},
			Expected: 2.0,
		},
		{
			Name: "Binary function",
			Input: &EvaluationTree{
				symbol:   FUNCTIONAL,
				operator: makeFunctionStage(ageAt),
				right: &EvaluationTree{
					symbol:   LITERAL,
					operator: makeLiteralStage([]string{"1955-06-29", "2006-01-02"}),
				},
			},
			Expected: 50.0,
		},
		{
			Name: "Notnull function",
			Input: &EvaluationTree{
				symbol:   FUNCTIONAL,
				operator: makeFunctionStage(notnull),
				right: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeAccessorStage([]string{"person", "Address"}),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: deceased,
				},
			},
			Expected: false,
		},
		{
			Name: "Null function",
			Input: &EvaluationTree{
				symbol:   FUNCTIONAL,
				operator: makeFunctionStage(null),
				right: &EvaluationTree{
					symbol:   ACCESS,
					operator: makeAccessorStage([]string{"person", "Address"}),
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "person",
					Value: deceased,
				},
			},
			Expected: true,
		},
	}
	runASTEvaluationTests(evaluationTests, test)
}

func runASTEvaluationTests(evaluationTests []astTest, test *testing.T) {

	var expression *EvaluableExpression
	var result interface{}
	var parameters map[string]interface{}
	var err error

	fmt.Printf("Running %d evaluation test cases...\n", len(evaluationTests))

	// Run the test cases.
	for _, evaluationTest := range evaluationTests {

		if evaluationTest.Functions != nil {
			expression, err = NewEvaluableExpressionFromASTWithFunctions(evaluationTest.Input, evaluationTest.Functions)
		} else {
			expression, err = NewEvaluableExpressionFromAST(evaluationTest.Input)
		}

		if err != nil {

			test.Logf("Test '%s' failed to parse: '%s'", evaluationTest.Name, err)
			test.Fail()
			continue
		}

		parameters = make(map[string]interface{}, 8)

		for _, parameter := range evaluationTest.Parameters {
			parameters[parameter.Name] = parameter.Value
		}

		result, err = expression.Evaluate(parameters)

		if err != nil {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		if result != evaluationTest.Expected {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, evaluationTest.Expected)
			test.Fail()
		}
	}
}
