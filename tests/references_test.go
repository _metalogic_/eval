package eval

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/_metalogic_/eval"
)

// refTest represents a test of Reference
type refTest struct {
	Name     string
	Input    interface{}
	Expected eval.Reference
}

var (
	personRef = eval.Reference{
		Name:     "Person",
		Type:     "struct",
		Nullable: true,
		References: []eval.Reference{
			{
				Name: "Name",
				Type: "string",
			},
			{
				Name:     "Address",
				Type:     "struct",
				Nullable: true,
				References: []eval.Reference{
					{
						Name: "Street",
						Type: "string",
					},
					{
						Name: "City",
						Type: "string",
					},
					{
						Name: "Region",
						Type: "string",
					},
					{
						Name: "Country",
						Type: "string",
					},
				},
			},
			{
				Name: "Birthdate",
				Type: "date",
			},
			{
				Name: "TimeOfBirth",
				Type: "datetime",
			},
			{
				Name: "Age",
				Type: "func(*eval.Person) int",
			},
			{
				Name: "AgeAt",
				Type: "func(*eval.Person, time.Time) int",
			},
			{
				Name: "AgeAtString",
				Type: "func(*eval.Person, string) int",
			},
			{
				Name: "CountryIn",
				Type: "func(*eval.Person, ...string) bool",
			},
			{
				Name: "IsResident",
				Type: "func(*eval.Person) bool",
			},
		},
	}
	birthInfoRef = eval.Reference{
		Name:     "BirthInfo",
		Type:     "struct",
		Nullable: true,
		References: []eval.Reference{
			{
				Name: "Birthdate",
				Type: "date",
			},
			{
				Name: "Birthtime",
				Type: "datetime",
			},
			{
				Name: "BirthdatePTR",
				Type: "date",
			},
			{
				Name: "BirthtimePTR",
				Type: "datetime",
			},
			{
				Name: "IsLeap",
				Type: "func(*eval.BirthInfo) bool",
			},
		},
	}

	ricky = "Ricky"
	peace = 50
)

func TestReferences(test *testing.T) {

	refTests := []refTest{

		{
			Name:     "Person",
			Input:    person,
			Expected: personRef,
		},
		{
			Name:     "BirthInfo",
			Input:    birthInfo,
			Expected: birthInfoRef,
		},
		{
			Name:     "Peace",
			Input:    peace,
			Expected: eval.Reference{Name: "Peace", Type: "int"},
		},
		{
			Name:     "Ricky",
			Input:    ricky,
			Expected: eval.Reference{Name: "Ricky", Type: "string"},
		},
	}

	runRefTests(refTests, test)

}

func runRefTests(refTests []refTest, test *testing.T) {

	var result eval.Reference

	fmt.Printf("Running %d evaluation test cases...\n", len(refTests))

	// Run the test cases.
	for _, refTest := range refTests {
		result = eval.References(refTest.Name, refTest.Input)
		if !reflect.DeepEqual(result, refTest.Expected) {
			test.Logf("Test '%s' failed", refTest.Name)
			resultJSON, _ := json.MarshalIndent(result, "", "  ")
			expectedJSON, _ := json.MarshalIndent(refTest.Expected, "", "  ")
			test.Logf("Ref test result '%s' does not match expected: '%+v'", string(resultJSON), string(expectedJSON))
			test.Fail()
		}
	}
}
