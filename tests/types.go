package eval

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/_metalogic_/glib/date"
)

type Person struct {
	Name        string    `json:"name"`
	Address     *Address  `json:"address"`
	Birthdate   date.Date `json:"birthdate"`
	TimeOfBirth time.Time `json:"timeOfBirth"`
}

func (p *Person) Age() int {
	return date.Age(p.Birthdate.DateTime())
}

func (p *Person) AgeAtString(d string) int {
	t, _ := time.Parse("2006-01-02", d)
	return date.AgeAt(p.Birthdate.DateTime(), t)
}

func (p *Person) AgeAt(t time.Time) int {
	return date.AgeAt(p.Birthdate.DateTime(), t)
}

func (p *Person) CountryIn(countries ...string) bool {
	for _, country := range countries {
		if p.Address != nil && p.Address.Country == country {
			return true
		}
	}
	return false
}

type Address struct {
	Street  string `json:"street"`
	City    string `json:"city"`
	Region  string `json:"region"`
	Country string `json:"country"`
}

func (p Person) IsResident() bool {
	return p.Address.Country == "Canada"
}

type BirthInfo struct {
	Birthdate    date.Date  `json:"birthdate"`
	Birthtime    time.Time  `json:"birthtime"`
	BirthdatePTR *date.Date `json:"birthdate-ptr"`
	BirthtimePTR *time.Time `json:"birthtime-ptr"`
}

func (b *BirthInfo) IsLeap() bool {
	return date.IsLeap(b.Birthtime)
}

var (
	person   *Person
	nofixed  *Person
	deceased *Person

	birthInfo *BirthInfo

	valentines2022 = 1.6447968e+18
)

func init() {

	birthJSON := `{
		"name": "Jane Smith",
		"address": {
			"street": "123 Main Street",
			"city": "Victoria",
			"region": "British Columbia",
			"country":"Canada"
		},
		"birthdate": "1955-06-29",
		"timeOfBirth": "1955-06-29T11:15:00Z"
	}`

	birthInfo = &BirthInfo{}

	err := json.Unmarshal([]byte(birthJSON), birthInfo)
	if err != nil {
		log.Fatal(err)
	}

	personJSON := `{
		"name": "Jane Smith",
		"address": {
			"street": "123 Main Street",
			"city": "Victoria",
			"region": "British Columbia",
			"country":"Canada"
		},
		"birthdate": "1955-06-29",
		"timeOfBirth": "1955-06-29T11:15:00Z"
	}`

	person = &Person{}

	err = json.Unmarshal([]byte(personJSON), person)
	if err != nil {
		log.Fatal(err)
	}

	nofixed = &Person{
		Name: "John Doe",
		Address: &Address{
			City:    "Victoria",
			Region:  "British Columbia",
			Country: "Canada",
		},
	}
	deceased = &Person{
		Name: "Jane Doe",
	}

}
