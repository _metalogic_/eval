package eval

import (
	"fmt"
	"testing"

	"bitbucket.org/_metalogic_/eval"
)

// astTest represents a test of expression evaluation
type jsonTest struct {
	Name       string
	Input      string
	Functions  map[string]eval.ExpressionFunction
	Parameters []eval.EvaluationParameter
	Expected   interface{}
}

func TestJSONEvaluation(test *testing.T) {

	arithTests := []jsonTest{

		{
			Name: "Single PLUS",
			Input: `{
				"type": "OPERATOR", 
				"value": "PLUS", 
				"left" : {
					"type": "LITERAL",
					"value": 49.0
				},
				"right": {
					"type": "LITERAL",
					"value": 51.0
				}
			}`,
			Expected: 100.0,
		},
		{
			Name: "Single MINUS",
			Input: `{
				"type": "OPERATOR", 
				"value": "MINUS", 
				"left" : {
					"type": "LITERAL",
					"value": 100.0
				},
				"right": {
					"type": "LITERAL",
					"value": 51.0
				}
			}`,
			Expected: 49.0,
		},
		{

			Name: "Single MULTIPLY",
			Input: `{
				"type": "OPERATOR", 
				"value": "MULTIPLY", 
				"left" : {
					"type": "LITERAL",
					"value": 5.0
				},
				"right": {
					"type": "LITERAL",
					"value": 20.0
				}
			}`,
			Expected: 100.0,
		},
		{

			Name: "Single DIVIDE",
			Input: `{
				"type": "OPERATOR", 
				"value": "DIVIDE", 
				"left" : {
					"type": "LITERAL",
					"value": 100.0
				},
				"right": {
					"type": "LITERAL",
					"value": 20.0
				}
			}`,
			Expected: 5.0,
		},
		{

			Name: "Single even MODULUS",
			Input: `{
				"type": "OPERATOR", 
				"value": "MODULUS", 
				"left" : {
					"type": "LITERAL",
					"value": 100.0
				},
				"right": {
					"type": "LITERAL",
					"value": 20.0
				}
			}`,
			Expected: 0.0,
		},
		{

			Name: "Single odd MODULUS",
			Input: `{
				"type": "OPERATOR", 
				"value": "MODULUS", 
				"left" : {
					"type": "LITERAL",
					"value": 101.0
				},
				"right": {
					"type": "LITERAL",
					"value": 2.0
				}
			}`,
			Expected: 1.0,
		},
	}

	runJSONEvaluationTests(arithTests, test)

	boolTests := []jsonTest{
		{
			Name: "TRUE",
			Input: `{
				"type": "OPERATOR", 
				"value": "OR", 
				"left" : {
					"type": "LITERAL",
					"value": true
				}
			}`,

			Expected: true,
		},
		{
			Name: "FALSE",
			Input: `{
				"type": "OPERATOR", 
				"value": "AND", 
				"left" : {
					"type": "LITERAL",
					"value": false
				}
			}`,

			Expected: false,
		},
		{
			Name: "Simple GT",
			Input: `{
				"type": "OPERATOR", 
				"value": "GT", 
				"left" : {
					"type": "LITERAL",
					"value": 49.0
				},
				"right": {
					"type": "LITERAL",
					"value": 51.0
				}
			}`,

			Expected: false,
		},
		{
			Name: "Simple EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "LITERAL",
					"value": 100.0
				},
				"right": {
					"type": "LITERAL",
					"value": 100.0
				}
			}`,
			Expected: true,
		},
		{
			Name: "Complex Boolean",
			Input: `{
				"type": "OPERATOR",
				"value": "AND",
				"left": {
					"type": "LITERAL",
					"value": true
				},
				"right": {
					"type": "OPERATOR", 
					"value": "EQ", 
					"left" : {
						"type": "LITERAL",
						"value": 100.0
					},
					"right": {
						"type": "OPERATOR", 
						"value": "PLUS", 
						"left" : {
							"type": "LITERAL",
							"value": 50.0
						},
						"right": {
							"type": "LITERAL",
							"value": 50.0
						}
					}
				}
			}`,
			Expected: true,
		},
	}

	runJSONEvaluationTests(boolTests, test)

	refTests := []jsonTest{

		{
			Name: "Reference GTE",
			Input: `{
				"type": "OPERATOR", 
				"value": "GTE", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Age"
				},
				"right": {
					"type": "LITERAL",
					"value": 65.0
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},

			Expected: true,
		},
		{
			Name: "Method Reference with one argument (A)",
			Input: `{
				"type": "OPERATOR",
				"value": "GTE",
				"left" : {
					"type": "REFERENCE",
					"value": "person.AgeAt",
					"right": {
						"type": "DATETIME",
						"value": "1964-12-31T11:59:59Z"
					}
				},
				"right": {
					"type": "LITERAL",
					"value": 9.0
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},

			Expected: true,
		},
		{
			Name: "Method Reference with one argument (B)",
			Input: `{
				"type": "OPERATOR",
				"value": "LT",
				"left" : {
					"type": "REFERENCE",
					"value": "person.AgeAt",
					"right": {
						"type": "DATETIME",
						"value": "1965-06-28T11:59:59Z"
					}
				},
				"right": {
					"type": "LITERAL",
					"value": 10.0
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},

			Expected: true,
		},
		{
			Name: "Reference EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Name"
				},
				"right": {
					"type": "LITERAL",
					"value": "Jane Smith"
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Reference IN",
			Input: `{
				"type": "OPERATOR", 
				"value": "IN", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Address.City"
				},
				"right": {
					"type": "LITERAL",
					"value": ["Vancouver","Victoria"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Reference IN",
			Input: `{
				"type": "OPERATOR", 
				"value": "IN", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Address.City"
				},
				"right": {
					"type": "LITERAL",
					"value": ["Chilliwack","Quesnel"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: false,
		},
		{
			Name: "Reference IN",
			Input: `{
				"type": "OPERATOR", 
				"value": "IN", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Age"
				},
				"right": {
					"type": "LITERAL",
					"value": [62,63,64,65,66,67]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Reference IN",
			Input: `{
				"type": "OPERATOR", 
				"value": "IN", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Age"
				},
				"right": {
					"type": "LITERAL",
					"value": [52,53,54]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: false,
		},
		{
			Name: "Reference Varargs",
			Input: `{
				"type": "REFERENCE",
				"value": "person.CountryIn",
				"right": {
					"type": "LITERAL",
					"value": ["Canada","China"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Reference Varargs",
			Input: `{
				"type": "REFERENCE",
				"value": "person.CountryIn",
				"right": {
					"type": "LITERAL",
					"value": ["Belgium","China"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: false,
		},
		{
			Name: "Deep Reference EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Address.City"
				},
				"right": {
					"type": "LITERAL",
					"value": "Victoria"
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Deep Nil Reference EQ",
			Input: `{
				"type": "OPERATOR",
				"value": "AND",
				"left": {
					"type": "OPERATOR",
					"value": "AND",
					"left": {
						"type": "FUNCTION",
						"value": "notnull",
						"right" : {
							"type": "REFERENCE",
							"value": "person.Address"						
						}
					},
					"right": {
						"type": "FUNCTION",
						"value": "notnull",
						"right" : {
							"type": "REFERENCE",
							"value": "person.Address.City"						
						}
					}
				},
				"right": {
					"type": "OPERATOR", 
					"value": "EQ", 
					"left" : {
						"type": "REFERENCE",
						"value": "person.Address.City"
					},
					"right": {
						"type": "LITERAL",
						"value": "Victoria"
					}
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: deceased,
				},
			},
			Expected: false,
		},
		{
			Name: "Empty Reference",
			Input: `{
				"type": "FUNCTION",
				"value": "empty",
				"right" : {
					"type": "REFERENCE",
					"value": "person.Address.Street"						
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: nofixed,
				},
			},
			Expected: true,
		},

		{
			Name: "Reference Date EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.Birthdate"
				},
				"right": {
					"type": "DATE",
					"value": "1955-06-29"
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "Reference Datetime EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "REFERENCE",
					"value": "person.TimeOfBirth"
				},
				"right": {
					"type": "DATE",
					"value": "1955-06-29T11:15:00Z"
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
	}

	runJSONEvaluationTests(refTests, test)

	funcTests := []jsonTest{

		{
			Name: "one() < 2",
			Input: `{
				"type": "OPERATOR", 
				"value": "LT", 
				"left" : {
					"type": "FUNCTION",
					"value": "one"
				},
				"right": {
					"type": "LITERAL",
					"value": 2.0
				}
			}`,

			Expected: true,
		},
		{
			Name: "round(1.5) == 2",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "FUNCTION",
					"value": "round",
					"right": {
						"type": "LITERAL",
						"value": 1.5
					}
				},
				"right": {
					"type": "LITERAL",
					"value": 2.0
				}
			}`,
			Expected: true,
		},
		{
			Name: "ageAt('1955-06-29','2006-01-02')",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "FUNCTION",
					"value": "ageAt",
					"right": {
						"type": "LITERAL",
						"value": ["1955-06-29","2006-01-02"]
					}
				},
				"right": {
					"type": "LITERAL",
					"value": 50.0
				}
			}`,
			Expected: true,
		},
		// The following is failing:
		// panic: interface conversion: interface {} is time.Time, not float64
		// now() returns float64, but DATETIME returns time.Time
		// {
		// 	Name: "now()",
		// 	Input: `{
		// 		"type": "OPERATOR",
		// 		"value": "GT",
		// 		"left" : {
		// 			"type": "FUNCTION",
		// 			"value": "now"
		// 		},
		// 		"right": {
		// 			"type": "DATETIME",
		// 			"value": "2022-01-01"
		// 		}
		// 	}`,
		// 	Expected: true,
		// },

		// do not use GT but rather use after/before:
		{
			Name: "after(2022-01-01)",
			Input: `{
				"type": "FUNCTION",
				"value": "after",
				"right": {
					"type": "DATETIME",
					"value": "2022-01-01"
				}
			}`,
			Expected: false,
		},
		{
			Name: "before(2022-01-01)",
			Input: `{
				"type": "FUNCTION",
				"value": "before",
				"right": {
					"type": "DATETIME",
					"value": "2022-01-01"
				}
			}`,
			Expected: true,
		},
		{
			Name: "startsWith(\"person.Name\", \"Sm\")",
			Input: `{
				"type": "OPERATOR",
				"value": "STARTSWITH_IN",
				"left": {
					"type": "REFERENCE",
					"value": "person.Name"
				},
				"right": {
					"type": "LITERAL",
					"value": ["Ja", "Mo", "Sm"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
		{
			Name: "endsWith(\"person.Name\", \"th\")",
			Input: `{
				"type": "OPERATOR",
				"value": "ENDSWITH_IN",
				"left": {
					"type": "REFERENCE",
					"value": "person.Name"
				},
				"right": {
					"type": "LITERAL",
					"value": ["gor", "son", "th"]
				}
			}`,
			Parameters: []eval.EvaluationParameter{
				{
					Name:  "person",
					Value: person,
				},
			},
			Expected: true,
		},
	}

	runJSONEvaluationTests(funcTests, test)

	malformedTests := []jsonTest{
		{
			Name:  "EMPTY",
			Input: ``,

			Expected: true,
		},
		{
			Name:     "UGLY",
			Input:    `{"type":"operator","value":"or","left": {"type":"LITERAL","value":false,"left":{"type":"operator","value":"=","left":{"type":"reference","value":"applicant.Age"},\"right":{"type":"literal","value":"30"}}}}`,
			Expected: false,
		},
		{
			Name:  "BAD",
			Input: `{"type":"operator","value":"or","left":{"type":"operator","value":"or","left":{"type":"reference","value":"application.ProgramIn","right":{"type":"literal","value":["Nursing"]}},"right":{"type":"operator","value":"EQ","left":{"type":"reference","value":"application.programInfo.programOffering2.StartDate"},"right":{"type":"literal","value":"01/06/2022"}}}}`,

			Expected: false,
		},
		{
			Name: "Simple EQ",
			Input: `{
				"type": "OPERATOR", 
				"value": "EQ", 
				"left" : {
					"type": "LITERAL",
					"value": 100.0
				},
				"right": {
					"type": "LITERAL",
					"value": 100.0
				}
			}`,
			Expected: true,
		},
		{
			Name: "Complex Boolean",
			Input: `{
				"type": "OPERATOR",
				"value": "AND",
				"left": {
					"type": "LITERAL",
					"value": true
				},
				"right": {
					"type": "OPERATOR", 
					"value": "EQ", 
					"left" : {
						"type": "LITERAL",
						"value": 100.0
					},
					"right": {
						"type": "OPERATOR", 
						"value": "PLUS", 
						"left" : {
							"type": "LITERAL",
							"value": 50.0
						},
						"right": {
							"type": "LITERAL",
							"value": 50.0
						}
					}
				}
			}`,
			Expected: true,
		},
	}

	runJSONEvaluationTests(malformedTests, test)

}

func runJSONEvaluationTests(tests []jsonTest, test *testing.T) {

	var expression *eval.EvaluableExpression
	var result interface{}
	var parameters map[string]interface{}
	var err error

	fmt.Printf("Running %d JSON evaluation test cases...\n", len(tests))

	// Run the test cases.
	for _, evaluationTest := range tests {

		expression, err = eval.NewEvaluableExpressionFromJSON([]byte(evaluationTest.Input))

		if err != nil {
			test.Logf("Test '%s' failed to parse: '%s'", evaluationTest.Name, err)
			test.Fail()
			continue
		}

		parameters = make(map[string]interface{}, 8)

		for _, parameter := range evaluationTest.Parameters {
			parameters[parameter.Name] = parameter.Value
		}

		result, err = expression.Evaluate(parameters)

		if err != nil {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		if result != evaluationTest.Expected {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, evaluationTest.Expected)
			test.Fail()
		}
	}
}
