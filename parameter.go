package eval

type EvaluationParameter struct {
	Name  string
	Value interface{}
}

