package eval

import (
	"errors"
	"fmt"
	"regexp"
	"testing"
	"time"
)

func TestExpressionNoParameterEvaluation(test *testing.T) {

	evaluationTests := []astTest{

		{

			Name:       "Single PLUS",
			Expression: "51 + 49",
			Expected:   100.0,
		},
		{

			Name:       "Single MINUS",
			Expression: "100 - 51",
			Expected:   49.0,
		},
		{

			Name:       "Single BITWISE AND",
			Expression: "100 & 50",
			Expected:   32.0,
		},
		{

			Name:       "Single BITWISE OR",
			Expression: "100 | 50",
			Expected:   118.0,
		},
		{

			Name:       "Single BITWISE XOR",
			Expression: "100 ^ 50",
			Expected:   86.0,
		},
		{

			Name:       "Single shift left",
			Expression: "2 << 1",
			Expected:   4.0,
		},
		{

			Name:       "Single shift right",
			Expression: "2 >> 1",
			Expected:   1.0,
		},
		{

			Name:       "Single BITWISE NOT",
			Expression: "~10",
			Expected:   -11.0,
		},
		{

			Name:       "Single MULTIPLY",
			Expression: "5 * 20",
			Expected:   100.0,
		},
		{

			Name:       "Single DIVIDE",
			Expression: "100 / 20",
			Expected:   5.0,
		},
		{

			Name:       "Single even MODULUS",
			Expression: "100 % 2",
			Expected:   0.0,
		},
		{

			Name:       "Single odd MODULUS",
			Expression: "101 % 2",
			Expected:   1.0,
		},
		{

			Name:       "Single EXPONENT",
			Expression: "10 ** 2",
			Expected:   100.0,
		},
		{

			Name:       "Compound PLUS",
			Expression: "20 + 30 + 50",
			Expected:   100.0,
		},
		{

			Name:       "Compound BITWISE AND",
			Expression: "20 & 30 & 50",
			Expected:   16.0,
		},
		{

			Name:       "Mutiple operators",
			Expression: "20 * 5 - 49",
			Expected:   51.0,
		},
		{

			Name:       "Parenthesis usage",
			Expression: "100 - (5 * 10)",
			Expected:   50.0,
		},
		{

			Name:       "Nested parentheses",
			Expression: "50 + (5 * (15 - 5))",
			Expected:   100.0,
		},
		{

			Name:       "Nested parentheses with bitwise",
			Expression: "100 ^ (23 * (2 | 5))",
			Expected:   197.0,
		},
		{

			Name:       "Logical OR operation of two clauses",
			Expression: "(1 == 1) || (true == true)",
			Expected:   true,
		},
		{

			Name:       "Logical AND operation of two clauses",
			Expression: "(1 == 1) && (true == true)",
			Expected:   true,
		},
		{

			Name:       "Implicit boolean",
			Expression: "2 > 1",
			Expected:   true,
		},
		{

			Name:       "Compound boolean",
			Expression: "5 < 10 && 1 < 5",
			Expected:   true,
		},
		{

			Name:       "Evaluated true && false operation (for issue #8)",
			Expression: "1 > 10 && 11 > 10",
			Expected:   false,
		},
		{

			Name:       "Evaluated true && false operation (for issue #8)",
			Expression: "true == true && false == true",
			Expected:   false,
		},
		{

			Name:       "Parenthesis boolean",
			Expression: "10 < 50 && (1 != 2 && 1 > 0)",
			Expected:   true,
		},
		{

			Name:       "Comparison of string constants",
			Expression: "'foo' == 'foo'",
			Expected:   true,
		},
		{

			Name:       "NEQ comparison of string constants",
			Expression: "'foo' != 'bar'",
			Expected:   true,
		},
		{

			Name:       "REQ comparison of string constants",
			Expression: "'foobar' =~ 'oba'",
			Expected:   true,
		},
		{

			Name:       "REQ comparison of string constants",
			Expression: "'foobar' =~ 'obas'",
			Expected:   false,
		},
		{

			Name:       "NREQ comparison of string constants",
			Expression: "'foo' !~ 'bar'",
			Expected:   true,
		},
		{

			Name:       "Multiplicative/additive order",
			Expression: "5 + 10 * 2",
			Expected:   25.0,
		},
		{

			Name:       "Multiple constant multiplications",
			Expression: "10 * 10 * 10",
			Expected:   1000.0,
		},
		{

			Name:       "Multiple adds/multiplications",
			Expression: "10 * 10 * 10 + 1 * 10 * 10",
			Expected:   1100.0,
		},
		{

			Name:       "Modulus precedence",
			Expression: "1 + 101 % 2 * 5",
			Expected:   6.0,
		},
		{

			Name:       "Exponent precedence",
			Expression: "1 + 5 ** 3 % 2 * 5",
			Expected:   6.0,
		},
		{

			Name:       "Bit shift precedence",
			Expression: "50 << 1 & 90",
			Expected:   64.0,
		},
		{

			Name:       "Bit shift precedence",
			Expression: "90 & 50 << 1",
			Expected:   64.0,
		},
		{

			Name:       "Bit shift precedence amongst non-bitwise",
			Expression: "90 + 50 << 1 * 5",
			Expected:   4480.0,
		},
		{
			Name:       "Order of non-commutative same-precedence operators (additive)",
			Expression: "1 - 2 - 4 - 8",
			Expected:   -13.0,
		},
		{
			Name:       "Order of non-commutative same-precedence operators (multiplicative)",
			Expression: "1 * 4 / 2 * 8",
			Expected:   16.0,
		},
		{
			Name:       "Null coalesce precedence",
			Expression: "true ?? true ? 100 + 200 : 400",
			Expected:   300.0,
		},
		{

			Name:       "Identical date equivalence",
			Expression: "'2014-01-02 14:12:22' == '2014-01-02 14:12:22'",
			Expected:   true,
		},
		{

			Name:       "Positive date GT",
			Expression: "'2014-01-02 14:12:22' > '2014-01-02 12:12:22'",
			Expected:   true,
		},
		{

			Name:       "Negative date GT",
			Expression: "'2014-01-02 14:12:22' > '2014-01-02 16:12:22'",
			Expected:   false,
		},
		{

			Name:       "Positive date GTE",
			Expression: "'2014-01-02 14:12:22' >= '2014-01-02 12:12:22'",
			Expected:   true,
		},
		{

			Name:       "Negative date GTE",
			Expression: "'2014-01-02 14:12:22' >= '2014-01-02 16:12:22'",
			Expected:   false,
		},
		{

			Name:       "Positive date LT",
			Expression: "'2014-01-02 14:12:22' < '2014-01-02 16:12:22'",
			Expected:   true,
		},
		{

			Name:       "Negative date LT",
			Expression: "'2014-01-02 14:12:22' < '2014-01-02 11:12:22'",
			Expected:   false,
		},
		{

			Name:       "Positive date LTE",
			Expression: "'2014-01-02 09:12:22' <= '2014-01-02 12:12:22'",
			Expected:   true,
		},
		{

			Name:       "Negative date LTE",
			Expression: "'2014-01-02 14:12:22' <= '2014-01-02 11:12:22'",
			Expected:   false,
		},
		{

			Name:       "Sign prefix comparison",
			Expression: "-1 < 0",
			Expected:   true,
		},
		{

			Name:       "Lexicographic LT",
			Expression: "'ab' < 'abc'",
			Expected:   true,
		},
		{

			Name:       "Lexicographic LTE",
			Expression: "'ab' <= 'abc'",
			Expected:   true,
		},
		{

			Name:       "Lexicographic GT",
			Expression: "'aba' > 'abc'",
			Expected:   false,
		},
		{

			Name:       "Lexicographic GTE",
			Expression: "'aba' >= 'abc'",
			Expected:   false,
		},
		{

			Name:       "Boolean sign prefix comparison",
			Expression: "!true == false",
			Expected:   true,
		},
		{

			Name:       "Inversion of clause",
			Expression: "!(10 < 0)",
			Expected:   true,
		},
		{

			Name:       "Negation after modifier",
			Expression: "10 * -10",
			Expected:   -100.0,
		},
		{

			Name:       "Ternary with single boolean",
			Expression: "true ? 10",
			Expected:   10.0,
		},
		{

			Name:       "Ternary nil with single boolean",
			Expression: "false ? 10",
			Expected:   nil,
		},
		{

			Name:       "Ternary with comparator boolean",
			Expression: "10 > 5 ? 35.50",
			Expected:   35.50,
		},
		{

			Name:       "Ternary nil with comparator boolean",
			Expression: "1 > 5 ? 35.50",
			Expected:   nil,
		},
		{

			Name:       "Ternary with parentheses",
			Expression: "(5 * (15 - 5)) > 5 ? 35.50",
			Expected:   35.50,
		},
		{

			Name:       "Ternary precedence",
			Expression: "true ? 35.50 > 10",
			Expected:   true,
		},
		{

			Name:       "Ternary-else",
			Expression: "false ? 35.50 : 50",
			Expected:   50.0,
		},
		{

			Name:       "Ternary-else inside clause",
			Expression: "(false ? 5 : 35.50) > 10",
			Expected:   true,
		},
		{

			Name:       "Ternary-else (true-case) inside clause",
			Expression: "(true ? 1 : 5) < 10",
			Expected:   true,
		},
		{

			Name:       "Ternary-else before comparator (negative case)",
			Expression: "true ? 1 : 5 > 10",
			Expected:   1.0,
		},
		{

			Name:       "Nested ternaries (#32)",
			Expression: "(2 == 2) ? 1 : (true ? 2 : 3)",
			Expected:   1.0,
		},
		{

			Name:       "Nested ternaries, right case (#32)",
			Expression: "false ? 1 : (true ? 2 : 3)",
			Expected:   2.0,
		},
		{

			Name:       "Doubly-nested ternaries (#32)",
			Expression: "true ? (false ? 1 : (false ? 2 : 3)) : (false ? 4 : 5)",
			Expected:   3.0,
		},
		{

			Name:       "String to string concat",
			Expression: "'foo' + 'bar' == 'foobar'",
			Expected:   true,
		},
		{

			Name:       "String to float64 concat",
			Expression: "'foo' + 123 == 'foo123'",
			Expected:   true,
		},
		{

			Name:       "Float64 to string concat",
			Expression: "123 + 'bar' == '123bar'",
			Expected:   true,
		},
		{

			Name:       "String to date concat",
			Expression: "'foo' + '02/05/1970' == 'foobar'",
			Expected:   false,
		},
		{

			Name:       "String to bool concat",
			Expression: "'foo' + true == 'footrue'",
			Expected:   true,
		},
		{

			Name:       "Bool to string concat",
			Expression: "true + 'bar' == 'truebar'",
			Expected:   true,
		},
		{

			Name:       "Null coalesce left",
			Expression: "1 ?? 2",
			Expected:   1.0,
		},
		{

			Name:       "Array membership literals",
			Expression: "1 in (1, 2, 3)",
			Expected:   true,
		},
		{

			Name:       "Array membership literal with inversion",
			Expression: "!(1 in (1, 2, 3))",
			Expected:   false,
		},
		{

			Name:       "Logical operator reordering (#30)",
			Expression: "(true && true) || (true && false)",
			Expected:   true,
		},
		{

			Name:       "Logical operator reordering without parens (#30)",
			Expression: "true && true || true && false",
			Expected:   true,
		},
		{

			Name:       "Logical operator reordering with multiple OR (#30)",
			Expression: "false || true && true || false",
			Expected:   true,
		},
		{

			Name:       "Left-side multiple consecutive (should be reordered) operators",
			Expression: "(10 * 10 * 10) > 10",
			Expected:   true,
		},
		{

			Name:       "Three-part non-paren logical op reordering (#44)",
			Expression: "false && true || true",
			Expected:   true,
		},
		{

			Name:       "Three-part non-paren logical op reordering (#44), second one",
			Expression: "true || false && true",
			Expected:   true,
		},
		{

			Name:       "Logical operator reordering without parens (#45)",
			Expression: "true && true || false && false",
			Expected:   true,
		},
		{

			Name:       "Single function",
			Expression: "foo()",
			Functions: map[string]ExpressionFunction{
				"foo": func(arguments ...interface{}) (interface{}, error) {
					return true, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Function with argument",
			Expression: "passthrough(1)",
			Functions: map[string]ExpressionFunction{
				"passthrough": func(arguments ...interface{}) (interface{}, error) {
					return arguments[0], nil
				},
			},

			Expected: 1.0,
		},

		{

			Name:       "Function with arguments",
			Expression: "passthrough(1, 2)",
			Functions: map[string]ExpressionFunction{
				"passthrough": func(arguments ...interface{}) (interface{}, error) {
					return arguments[0].(float64) + arguments[1].(float64), nil
				},
			},

			Expected: 3.0,
		},
		{

			Name:       "Nested function with precedence",
			Expression: "sum(1, sum(2, 3), 2 + 2, true ? 4 : 5)",
			Functions: map[string]ExpressionFunction{
				"sum": func(arguments ...interface{}) (interface{}, error) {

					sum := 0.0
					for _, v := range arguments {
						sum += v.(float64)
					}
					return sum, nil
				},
			},

			Expected: 14.0,
		},
		{

			Name:       "Empty function and modifier, compared",
			Expression: "numeric()-1 > 0",
			Functions: map[string]ExpressionFunction{
				"numeric": func(arguments ...interface{}) (interface{}, error) {
					return 2.0, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Empty function comparator",
			Expression: "numeric() > 0",
			Functions: map[string]ExpressionFunction{
				"numeric": func(arguments ...interface{}) (interface{}, error) {
					return 2.0, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Empty function logical operator",
			Expression: "success() && !false",
			Functions: map[string]ExpressionFunction{
				"success": func(arguments ...interface{}) (interface{}, error) {
					return true, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Empty function ternary",
			Expression: "nope() ? 1 : 2.0",
			Functions: map[string]ExpressionFunction{
				"nope": func(arguments ...interface{}) (interface{}, error) {
					return false, nil
				},
			},

			Expected: 2.0,
		},
		{

			Name:       "Empty function null coalesce",
			Expression: "null() ?? 2",
			Functions: map[string]ExpressionFunction{
				"null": func(arguments ...interface{}) (interface{}, error) {
					return nil, nil
				},
			},

			Expected: 2.0,
		},
		{

			Name:       "Empty function with prefix",
			Expression: "-ten()",
			Functions: map[string]ExpressionFunction{
				"ten": func(arguments ...interface{}) (interface{}, error) {
					return 10.0, nil
				},
			},

			Expected: -10.0,
		},
		{

			Name:       "Empty function as part of chain",
			Expression: "10 - numeric() - 2",
			Functions: map[string]ExpressionFunction{
				"numeric": func(arguments ...interface{}) (interface{}, error) {
					return 5.0, nil
				},
			},

			Expected: 3.0,
		},
		{

			Name:       "Empty function near separator",
			Expression: "10 in (1, 2, 3, ten(), 8)",
			Functions: map[string]ExpressionFunction{
				"ten": func(arguments ...interface{}) (interface{}, error) {
					return 10.0, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Enclosed empty function with modifier and comparator (#28)",
			Expression: "(ten() - 1) > 3",
			Functions: map[string]ExpressionFunction{
				"ten": func(arguments ...interface{}) (interface{}, error) {
					return 10.0, nil
				},
			},

			Expected: true,
		},
		{

			Name:       "Ternary/Java EL ambiguity",
			Expression: "false ? foo:length()",
			Functions: map[string]ExpressionFunction{
				"length": func(arguments ...interface{}) (interface{}, error) {
					return 1.0, nil
				},
			},
			Expected: 1.0,
		},
	}

	runExpressionEvaluationTests(evaluationTests, test)
}

func TestExpressionParameterizedEvaluation(test *testing.T) {

	evaluationTests := []astTest{

		{

			Name:       "Single parameter modified by constant",
			Expression: "foo + 2",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 2.0,
				},
			},
			Expected: 4.0,
		},
		{

			Name:       "Single parameter modified by variable",
			Expression: "foo * bar",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 5.0,
				},
				{
					Name:  "bar",
					Value: 2.0,
				},
			},
			Expected: 10.0,
		},
		{

			Name:       "Multiple multiplications of the same parameter",
			Expression: "foo * foo * foo",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 10.0,
				},
			},
			Expected: 1000.0,
		},
		{

			Name:       "Multiple additions of the same parameter",
			Expression: "foo + foo + foo",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 10.0,
				},
			},
			Expected: 30.0,
		},
		{

			Name:       "Parameter name sensitivity",
			Expression: "foo + FoO + FOO",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 8.0,
				},
				{
					Name:  "FoO",
					Value: 4.0,
				},
				{
					Name:  "FOO",
					Value: 2.0,
				},
			},
			Expected: 14.0,
		},
		{

			Name:       "Sign prefix comparison against prefixed variable",
			Expression: "-1 < -foo",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: -8.0,
				},
			},
			Expected: true,
		},
		{

			Name:       "Fixed-point parameter",
			Expression: "foo > 1",
			Parameters: []EvaluationParameter{

				{
					Name:  "foo",
					Value: 2,
				},
			},
			Expected: true,
		},
		{

			Name:       "Modifier after closing clause",
			Expression: "(2 + 2) + 2 == 6",
			Expected:   true,
		},
		{

			Name:       "Comparator after closing clause",
			Expression: "(2 + 2) >= 4",
			Expected:   true,
		},
		{

			Name:       "Two-boolean logical operation (for issue #8)",
			Expression: "(foo == true) || (bar == true)",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: true,
				},
				{
					Name:  "bar",
					Value: false,
				},
			},
			Expected: true,
		},
		{

			Name:       "Two-variable integer logical operation (for issue #8)",
			Expression: "foo > 10 && bar > 10",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 1,
				},
				{
					Name:  "bar",
					Value: 11,
				},
			},
			Expected: false,
		},
		{

			Name:       "Regex against right-hand parameter",
			Expression: "'foobar' =~ foo",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "obar",
				},
			},
			Expected: true,
		},
		{

			Name:       "Not-regex against right-hand parameter",
			Expression: "'foobar' !~ foo",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "baz",
				},
			},
			Expected: true,
		},
		{

			Name:       "Regex against two parameters",
			Expression: "foo =~ bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "foobar",
				},
				{
					Name:  "bar",
					Value: "oba",
				},
			},
			Expected: true,
		},
		{

			Name:       "Not-regex against two parameters",
			Expression: "foo !~ bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "foobar",
				},
				{
					Name:  "bar",
					Value: "baz",
				},
			},
			Expected: true,
		},
		{

			Name:       "Pre-compiled regex",
			Expression: "foo =~ bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "foobar",
				},
				{
					Name:  "bar",
					Value: regexp.MustCompile("[fF][oO]+"),
				},
			},
			Expected: true,
		},
		{

			Name:       "Pre-compiled not-regex",
			Expression: "foo !~ bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "foobar",
				},
				{
					Name:  "bar",
					Value: regexp.MustCompile("[fF][oO]+"),
				},
			},
			Expected: false,
		},
		{

			Name:       "Single boolean parameter",
			Expression: "commission ? 10",
			Parameters: []EvaluationParameter{
				{
					Name:  "commission",
					Value: true,
				},
			},
			Expected: 10.0,
		},
		{

			Name:       "True comparator with a parameter",
			Expression: "partner == 'amazon' ? 10",
			Parameters: []EvaluationParameter{
				{
					Name:  "partner",
					Value: "amazon",
				},
			},
			Expected: 10.0,
		},
		{

			Name:       "False comparator with a parameter",
			Expression: "partner == 'amazon' ? 10",
			Parameters: []EvaluationParameter{
				{
					Name:  "partner",
					Value: "ebay",
				},
			},
			Expected: nil,
		},
		{

			Name:       "True comparator with multiple parameters",
			Expression: "theft && period == 24 ? 60",
			Parameters: []EvaluationParameter{
				{
					Name:  "theft",
					Value: true,
				},
				{
					Name:  "period",
					Value: 24,
				},
			},
			Expected: 60.0,
		},
		{

			Name:       "False comparator with multiple parameters",
			Expression: "theft && period == 24 ? 60",
			Parameters: []EvaluationParameter{
				{
					Name:  "theft",
					Value: false,
				},
				{
					Name:  "period",
					Value: 24,
				},
			},
			Expected: nil,
		},
		{

			Name:       "String concat with single string parameter",
			Expression: "foo + 'bar'",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "baz",
				},
			},
			Expected: "bazbar",
		},
		{

			Name:       "String concat with multiple string parameter",
			Expression: "foo + bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "baz",
				},
				{
					Name:  "bar",
					Value: "quux",
				},
			},
			Expected: "bazquux",
		},
		{

			Name:       "String concat with float parameter",
			Expression: "foo + bar",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "baz",
				},
				{
					Name:  "bar",
					Value: 123.0,
				},
			},
			Expected: "baz123",
		},
		{

			Name:       "Mixed multiple string concat",
			Expression: "foo + 123 + 'bar' + true",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: "baz",
				},
			},
			Expected: "baz123bartrue",
		},
		{

			Name:       "Integer width spectrum",
			Expression: "uint8 + uint16 + uint32 + uint64 + int8 + int16 + int32 + int64",
			Parameters: []EvaluationParameter{
				{
					Name:  "uint8",
					Value: uint8(0),
				},
				{
					Name:  "uint16",
					Value: uint16(0),
				},
				{
					Name:  "uint32",
					Value: uint32(0),
				},
				{
					Name:  "uint64",
					Value: uint64(0),
				},
				{
					Name:  "int8",
					Value: int8(0),
				},
				{
					Name:  "int16",
					Value: int16(0),
				},
				{
					Name:  "int32",
					Value: int32(0),
				},
				{
					Name:  "int64",
					Value: int64(0),
				},
			},
			Expected: 0.0,
		},
		{

			Name:       "Floats",
			Expression: "float32 + float64",
			Parameters: []EvaluationParameter{
				{
					Name:  "float32",
					Value: float32(0.0),
				},
				{
					Name:  "float64",
					Value: float64(0.0),
				},
			},
			Expected: 0.0,
		},
		{

			Name:       "Null coalesce right",
			Expression: "foo ?? 1.0",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: nil,
				},
			},
			Expected: 1.0,
		},
		{

			Name:       "Multiple comparator/logical operators (#30)",
			Expression: "(foo >= 2887057408 && foo <= 2887122943) || (foo >= 168100864 && foo <= 168118271)",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 2887057409,
				},
			},
			Expected: true,
		},
		{

			Name:       "Multiple comparator/logical operators, opposite order (#30)",
			Expression: "(foo >= 168100864 && foo <= 168118271) || (foo >= 2887057408 && foo <= 2887122943)",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 2887057409,
				},
			},
			Expected: true,
		},
		{

			Name:       "Multiple comparator/logical operators, small value (#30)",
			Expression: "(foo >= 2887057408 && foo <= 2887122943) || (foo >= 168100864 && foo <= 168118271)",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 168100865,
				},
			},
			Expected: true,
		},
		{

			Name:       "Multiple comparator/logical operators, small value, opposite order (#30)",
			Expression: "(foo >= 168100864 && foo <= 168118271) || (foo >= 2887057408 && foo <= 2887122943)",
			Parameters: []EvaluationParameter{
				{
					Name:  "foo",
					Value: 168100865,
				},
			},
			Expected: true,
		},
		{

			Name:       "Incomparable array equality comparison",
			Expression: "arr == arr",
			Parameters: []EvaluationParameter{
				{
					Name:  "arr",
					Value: []int{0, 0, 0},
				},
			},
			Expected: true,
		},
		{

			Name:       "Incomparable array not-equality comparison",
			Expression: "arr != arr",
			Parameters: []EvaluationParameter{
				{
					Name:  "arr",
					Value: []int{0, 0, 0},
				},
			},
			Expected: false,
		},
		{

			Name:       "Mixed function and parameters",
			Expression: "sum(1.2, amount) + name",
			Functions: map[string]ExpressionFunction{
				"sum": func(arguments ...interface{}) (interface{}, error) {

					sum := 0.0
					for _, v := range arguments {
						sum += v.(float64)
					}
					return sum, nil
				},
			},
			Parameters: []EvaluationParameter{
				{
					Name:  "amount",
					Value: .8,
				},
				{
					Name:  "name",
					Value: "awesome",
				},
			},

			Expected: "2awesome",
		},
		{

			Name:       "Short-circuit OR",
			Expression: "true || fail()",
			Functions: map[string]ExpressionFunction{
				"fail": func(arguments ...interface{}) (interface{}, error) {
					return nil, errors.New("Did not short-circuit")
				},
			},
			Expected: true,
		},
		{

			Name:       "Short-circuit AND",
			Expression: "false && fail()",
			Functions: map[string]ExpressionFunction{
				"fail": func(arguments ...interface{}) (interface{}, error) {
					return nil, errors.New("Did not short-circuit")
				},
			},
			Expected: false,
		},
		{

			Name:       "Short-circuit ternary",
			Expression: "true ? 1 : fail()",
			Functions: map[string]ExpressionFunction{
				"fail": func(arguments ...interface{}) (interface{}, error) {
					return nil, errors.New("Did not short-circuit")
				},
			},
			Expected: 1.0,
		},
		{

			Name:       "Short-circuit coalesce",
			Expression: "'foo' ?? fail()",
			Functions: map[string]ExpressionFunction{
				"fail": func(arguments ...interface{}) (interface{}, error) {
					return nil, errors.New("Did not short-circuit")
				},
			},
			Expected: "foo",
		},
		{

			Name:       "Simple parameter call",
			Expression: "foo.String",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   fooParameter.Value.(dummyParameter).String,
		},
		{

			Name:       "Simple parameter function call",
			Expression: "foo.Func()",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "funk",
		},
		{

			Name:       "Simple parameter call from pointer",
			Expression: "fooptr.String",
			Parameters: []EvaluationParameter{fooPtrParameter},
			Expected:   fooParameter.Value.(dummyParameter).String,
		},
		{

			Name:       "Simple parameter function call from pointer",
			Expression: "fooptr.Func()",
			Parameters: []EvaluationParameter{fooPtrParameter},
			Expected:   "funk",
		},
		{

			Name:       "Simple parameter function call from pointer",
			Expression: "fooptr.Func3()",
			Parameters: []EvaluationParameter{fooPtrParameter},
			Expected:   "fronk",
		},
		{

			Name:       "Simple parameter call",
			Expression: "foo.String == 'hi'",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   false,
		},
		{

			Name:       "Simple parameter call with modifier",
			Expression: "foo.String + 'hi'",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   fooParameter.Value.(dummyParameter).String + "hi",
		},
		{

			Name:       "Simple parameter function call, two-arg return",
			Expression: "foo.Func2()",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "frink",
		},
		{

			Name:       "Parameter function call with all argument types",
			Expression: "foo.TestArgs(\"hello\", 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1.0, 2.0, true)",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "hello: 33",
		},

		{

			Name:       "Simple parameter function call, one arg",
			Expression: "foo.FuncArgStr('boop')",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "boop",
		},
		{

			Name:       "Simple parameter function call, one arg",
			Expression: "foo.FuncArgStr('boop') + 'hi'",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "boophi",
		},
		{

			Name:       "Nested parameter function call",
			Expression: "foo.Nested.Dunk('boop')",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "boopdunk",
		},
		{

			Name:       "Nested parameter call",
			Expression: "foo.Nested.Funk",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "funkalicious",
		},
		{

			Name:       "Parameter call with + modifier",
			Expression: "1 + foo.Int",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   102.0,
		},
		{

			Name:       "Parameter string call with + modifier",
			Expression: "'woop' + (foo.String)",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   "woopstring!",
		},
		{

			Name:       "Parameter call with && operator",
			Expression: "true && foo.BoolFalse",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   false,
		},
		{

			Name:       "Null coalesce nested parameter",
			Expression: "foo.Nil ?? false",
			Parameters: []EvaluationParameter{fooParameter},
			Expected:   false,
		},
	}

	runExpressionEvaluationTests(evaluationTests, test)
}

/*
	Tests the behavior of a nil set of parameters.
*/
func TestExpressionNilParameters(test *testing.T) {

	expression, _ := NewEvaluableExpression("true")
	_, err := expression.Evaluate(nil)

	if err != nil {
		test.Fail()
	}
}

/*
	Tests functionality related to using functions with a struct method receiver.
	Created to test #54.
*/
func TestExpressionStructFunctions(test *testing.T) {

	parseFormat := "2006"
	y2k, _ := time.Parse(parseFormat, "2000")
	y2k1, _ := time.Parse(parseFormat, "2001")

	functions := map[string]ExpressionFunction{
		"func1": func(args ...interface{}) (interface{}, error) {
			return float64(y2k.Year()), nil
		},
		"func2": func(args ...interface{}) (interface{}, error) {
			return float64(y2k1.Year()), nil
		},
	}

	exp, _ := NewEvaluableExpressionWithFunctions("func1() + func2()", functions)
	result, _ := exp.Evaluate(nil)

	if result != 4001.0 {
		test.Logf("Function calling method did not return the right value. Got: %v, expected %d\n", result, 4001)
		test.Fail()
	}
}

func runExpressionEvaluationTests(evaluationTests []astTest, test *testing.T) {

	var expression *EvaluableExpression
	var result interface{}
	var parameters map[string]interface{}

	fmt.Printf("Running %d evaluation test cases...\n", len(evaluationTests))

	// Run the test cases.
	for _, evaluationTest := range evaluationTests {
		tree, err := treeFromExpression(evaluationTest.Expression, evaluationTest.Functions)
		if err != nil {
			test.Logf("Test '%s' failed to get evaluation tree from expression %s: '%s'", evaluationTest.Name, evaluationTest.Expression, err)
			test.Fail()
			continue
		}

		evaluationTest.Input = tree

		//		fmt.Printf("%s: %s\n", evaluationTest.Name, tree)

		if evaluationTest.Functions != nil {
			expression, err = NewEvaluableExpressionFromASTWithFunctions(evaluationTest.Input, evaluationTest.Functions)
		} else {
			expression, err = NewEvaluableExpressionFromAST(evaluationTest.Input)
		}

		if err != nil {

			test.Logf("Test '%s' failed to parse: '%s'", evaluationTest.Name, err)
			test.Fail()
			continue
		}

		parameters = make(map[string]interface{}, 8)

		for _, parameter := range evaluationTest.Parameters {
			parameters[parameter.Name] = parameter.Value
		}

		result, err = expression.Evaluate(parameters)

		if err != nil {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		if result != evaluationTest.Expected {

			test.Logf("Test '%s' failed", evaluationTest.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, evaluationTest.Expected)
			test.Fail()
		}
	}
}
